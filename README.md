# README #

This is a starter template for a php and html5 website using slim, twig and a ORM framework for database connection.
It is build as a RESTful webservice.

### How do I get set up? ###
* To add the remote source for the project. 
* git remote add source https//lano78@bitbucket.org/lano78/php-slim-template.git
* git pull source master
* Add your repo remote
* git remote add origin https://....your bitbucket repo url...
* git pull origin master
* Configure the projects dev.php and prod.php located at app/config/
* Specify the mode in the mode.php
* To install the dependencies, run in cmd at projects root
* php composer.phar install
* php composer.phar dump-autoload -o
*
*
