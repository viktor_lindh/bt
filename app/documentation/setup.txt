Navigate to site root and run install command in the  terminal
php composer.phar install

Composer will look for the composer.json file and download all the dependencies.


To generate autoload files run this command with optimize flag ('-o').
php composer.phar dump-autoload -o