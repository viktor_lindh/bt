<?php
/**
 * Created by PhpStorm.
 * User: lars
 * Date: 2015-08-24
 * Time: 21:18
 */
return[

    'mode' => 'Development Mode',

    'app' => [
	'url' => 'http://localhost',
    ] ,

    'site' =>[
	'name' => 'En lite sida',
	'under_construction' => false,
	'beta' => true,
	'toplinks' => true,
	'avatar' => false
    ],

    'database' => [
	'driver' => 'mysql',
	'host' => '127.0.0.1',
	'name' => 'webbdb',
	'username' => 'root',
	'password' => '',
	'charset' => 'utf8',
	'collation' => 'utf8_unicode_ci',
	'prefix' => ''
    ],

    'log' => [
	'debug' => true
    ],
    'twig' => [
	'debug' => true
    ],
];